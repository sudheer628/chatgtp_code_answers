import pandas as pd
import numpy as np

def commodity_channel_index(df, n=14):
    "Calculates the Commodity Channel Index for given data."
    TP = (df['high'] + df['low'] + df['close']) / 3
    df['MA'] = TP.rolling(window=n).mean()
    df['MD'] = (TP - df['MA']).abs().rolling(window=n).mean()
    df['CCI'] = (TP - df['MA']) / (0.015 * df['MD'])
    return df[['CCI']]

# Example usage
df = pd.read_csv("stock_data.csv")
commodity_channel_index(df)

'''
Note: df is a pandas dataframe containing the following columns: 'date', 'open', 'high', 'low', 'close'.
You'll need to replace "stock_data.csv" with the name of your own data file.

Commodity Channel Index identifies new trends in the market. It has values of 0, +100, and -100. If the value is positive, it indicates uptrend, if the CCI is negative, it indicates that the market is in the downtrend. CCI is coupled with RSI to obtain information about overbought and oversold stocks.
'''
