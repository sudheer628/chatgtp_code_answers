import pandas as pd
import numpy as np

def bollinger_bands(df, window_size=20, num_of_std=2):
    "Calculates the Bollinger Bands for given data."
    rolling_mean = df['close'].rolling(window=window_size).mean()
    rolling_std  = df['close'].rolling(window=window_size).std()
    upper_band = rolling_mean + (rolling_std * num_of_std)
    lower_band = rolling_mean - (rolling_std * num_of_std)
    df['Bollinger Upper Band'] = upper_band
    df['Bollinger Lower Band'] = lower_band
    return df[['Bollinger Upper Band', 'Bollinger Lower Band']]

# Example usage
df = pd.read_csv("stock_data.csv")
bollinger_bands(df)


'''
Note: df is a pandas dataframe containing the following columns: 'date', 'open', 'high', 'low', 'close'.
You'll need to replace "stock_data.csv" with the name of your own data file.
Bollinger bands indicate the volatility in the market. Bollinger bands are of 3 types: a middle bang which is a 20-day simple moving average, a +2 standard deviation upper bang and a -2 lower deviation lower band. The price of a stock moves between the upper and the lower band. When the market is moving and the volatility is greater, the band widen and when the volatility is less the gap decreases. Bollinger bands help traders to understand the price range of a particular stock.
'''
