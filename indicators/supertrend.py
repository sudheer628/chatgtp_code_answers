import pandas as pd
import numpy as np

def supertrend(df, n, multiplier):
    df['basic_ust'] = (df['High'] + df['Low'])/2
    df['average'] = df['basic_ust'].rolling(n).mean()
    df['deviation'] = abs(df['basic_ust'] - df['average'])
    df['topband'] = df['average'] + (multiplier * df['deviation'])
    df['bottomband'] = df['average'] - (multiplier * df['deviation'])
    df['super_trend'] = 0.00
    df.loc[df['basic_ust'] > df['topband'], 'super_trend'] = df['topband']
    df.loc[df['basic_ust'] < df['bottomband'], 'super_trend'] = df['bottomband']
    for i in range(1, len(df)):
        if df.iloc[i]['super_trend'] == df.iloc[i]['bottomband']:
            df.at[df.index[i], 'super_trend'] = df.iloc[i-1]['topband']
        elif df.iloc[i]['super_trend'] == df.iloc[i]['topband']:
            df.at[df.index[i], 'super_trend'] = df.iloc[i-1]['bottomband']
        else:
            df.at[df.index[i], 'super_trend'] = df.iloc[i-1]['super_trend']
    df['super_trend_type'] = 'NaN'
    df.loc[df['basic_ust'] < df['super_trend'], 'super_trend_type'] = 'long'
    df.loc[df['basic_ust'] > df['super_trend'], 'super_trend_type'] = 'short'
    return df

# Example usage
df = pd.read_csv("data.csv")
df = supertrend(df, 7, 3)
print(df[['Date', 'Close', 'super_trend', 'super_trend_type']])


'''
In this code, the df dataframe contains the historical data for the stock. n is the number of days used to calculate the average, and multiplier is the factor used to calculate the deviation. The code returns a dataframe with the original data plus the calculated SuperTrend values and the long/short signals.
'''
