import pandas as pd
import numpy as np

def chaikin_money_flow(df, n=20):
    # Create the high and low range
    high_low_range = df['High'] - df['Low']
    
    # Create the money flow multiplier
    money_flow_multiplier = ((df['Close'] - df['Low']) - (df['High'] - df['Close'])) / (df['High'] - df['Low'])
    
    # Create the money flow volume
    money_flow_volume = money_flow_multiplier * df['Volume']
    
    # Create the chaikin money flow
    cmf = pd.Series(money_flow_volume, name='Chaikin Money Flow').rolling(window=n).mean() / df['Adj Close'].rolling(window=n).mean()
    
    return cmf

# Example usage:
df = pd.read_csv("<path to stock data>")
df['Adj Close'] = df['Close'] * df['Adj Close'] / df['Close']
cmf = chaikin_money_flow(df)
print(cmf)


'''
This code assumes that the stock data is stored in a CSV file with columns High, Low, Close, Volume, and Adj Close. The chaikin_money_flow function takes a dataframe df and a window size n (default is 20) as inputs and returns the CMF values for each day.
'''
