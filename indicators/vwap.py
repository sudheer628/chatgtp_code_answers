import pandas as pd

def vwap(df):
    df['cum_volume'] = df['Volume'].cumsum()
    df['cum_price_mul_volume'] = (df['Close'] * df['Volume']).cumsum()
    df['vwap'] = df['cum_price_mul_volume'] / df['cum_volume']
    return df

# Example usage
df = pd.read_csv("data.csv")
df = vwap(df)
print(df[['Date', 'Close', 'Volume', 'vwap']])

'''
In this code, the df dataframe contains the historical data for the stock. The code returns a dataframe with the original data plus the calculated VWAP values.
'''
