### code1 - just stochastic ind
import pandas as pd

def stochastics(df, n):
    df['L14'] = df['Low'].rolling(n).min()
    df['H14'] = df['High'].rolling(n).max()
    df['%K'] = 100 * (df['Close'] - df['L14']) / (df['H14'] - df['L14'])
    df['%D'] = df['%K'].rolling(3).mean()
    return df

df = pd.read_csv('stock_data.csv')
df = stochastics(df, 14)

'''
This code takes a dataframe df as input, which should contain columns for the closing price 'Close', high price 'High', and low price 'Low' of the stock. The stochastics function calculates the Stochastics indicator and returns the original dataframe with two new columns, '%K' and '%D', containing the Fast Stochastics and Slow Stochastics lines, respectively. In this example, the Stochastics indicator is calculated using a 14-day lookback period. The minimum low price over the past 14 days is calculated and stored in the 'L14' column, while the maximum high price over the past 14 days is stored in the 'H14' column. The Fast Stochastics line is calculated as the percentage of the close price relative to the range between the 14-day low and high. The Slow Stochastics line is the 3-day moving average of the Fast Stochastics line.
'''

#### code2 - stochastic oscilator ind
import pandas as pd
import numpy as np

def stochastic_oscillator(df, n=14):
    "Calculates the Stochastic Oscillator for given data."
    df = df.copy()
    df['L14'] = df['low'].rolling(window=n).min()
    df['H14'] = df['high'].rolling(window=n).max()
    df['%K'] = 100 * (df['close'] - df['L14']) / (df['H14'] - df['L14'])
    df['%D'] = df['%K'].rolling(window=3).mean()
    return df[['%K','%D']]

# Example usage
df = pd.read_csv("stock_data.csv")
stochastic_oscillator(df)

'''
Note: df is a pandas dataframe containing the following columns: 'date', 'open', 'high', 'low', 'close'.
You'll need to replace "stock_data.csv" with the name of your own data file.
'''
