import pandas as pd

def on_balance_volume(df):
    obv = [0]
    for i in range(1, len(df)):
        if df['Close'][i] > df['Close'][i-1]:
            obv.append(obv[-1] + df['Volume'][i])
        elif df['Close'][i] < df['Close'][i-1]:
            obv.append(obv[-1] - df['Volume'][i])
        else:
            obv.append(obv[-1])
    return pd.Series(obv, name='OBV')

df = pd.read_csv('stock_data.csv')
df['OBV'] = on_balance_volume(df)

'''
This code takes a dataframe df as input, which should contain columns for the closing price 'Close' and the volume 'Volume' of the stock. The on_balance_volume function calculates the OBV and returns it as a Pandas series. The OBV is calculated by adding the volume on days when the closing price increased, and subtracting the volume on days when the closing price decreased. The resulting series is added to the original dataframe as a new column named 'OBV'.
'''
