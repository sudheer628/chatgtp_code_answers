import pandas as pd

def donchian_channel(df, n):
    df['Donchian_High'] = df['High'].rolling(n).max()
    df['Donchian_Low'] = df['Low'].rolling(n).min()
    return df

df = pd.read_csv('stock_data.csv')
df = donchian_channel(df, 20)

'''
This code takes a dataframe df as input, which should contain columns for the high and low prices 'High' and 'Low' of the stock. The donchian_channel function calculates the Donchian Channel and returns the original dataframe with two new columns, 'Donchian_High' and 'Donchian_Low', containing the n-day rolling max and min of the high and low prices, respectively. In this example, the Donchian Channel is calculated using a 20-day lookback period.
'''
